
document.title = 'Visual Fibonacci Representation';

//The master div
var div = document.createElement('div');
div.setAttribute('class', 'red fib-container');
document.querySelector('body').appendChild(div);

//The master form
var form = document.createElement('form');
form.setAttribute('id', 'list-of-divs');

var label = document.createElement('label');
label.textContent = 'Fib (0)';
label.setAttribute('id', 'list-label');

//The input to place in the form that constructs the slider
var slider = document.createElement('input');
slider.setAttribute('type','range');
slider.setAttribute('min', '0');
slider.setAttribute('max', '11');
slider.setAttribute('value', '0');
slider.setAttribute('oninput', 'listSlider(this)');

form.appendChild(label);
form.appendChild(slider);

div.appendChild(form);

//The div to hold the fibonacci expression to be visually represented

var div2 = document.createElement('div');
div2.setAttribute('class', 'fib-container');
div2.setAttribute('id', 'tree-of-divs');
div.appendChild(div2);

//This div is to hold the item in div two so the style sheet works appropriately
var newDiv = document.createElement('div');
newDiv.setAttribute('class', 'fib-item');

var fibTxt = document.createElement('p');
fibTxt.textContent = 'Fib(0) = 0';
newDiv.appendChild(fibTxt);

div2.appendChild(newDiv);


var listSlider = function(sliderVal) {
    var label = document.querySelector('#list-label');
    label.textContent = `Fib(${sliderVal.value})`;
    
    
//    After every tree of divs is presented it needs be erased for the next slide position
//    
    var treeOfDivs = document.querySelector('#tree-of-divs');
    if (treeOfDivs) {
        treeOfDivs.remove();
    }

    
    treeOfDivs = document.createElement('div');
    treeOfDivs.setAttribute('id', 'tree-of-divs');
    treeOfDivs.setAttribute('class', 'fib-container');

    var treeObj = fibonacci(sliderVal.value).div;
    treeOfDivs.appendChild(treeObj);

    var tempForm = document.querySelector('#list-of-divs');
    tempForm.appendChild(treeOfDivs);
}


//Here is the fibonacci function
var fibonacci = function(treeDepth){
//Checking the base cases, using two as a base case seemed reduntant
    if (treeDepth == 0) {
        let a = {
            div: document.createElement('div'),
            val: 0
        }
        p = document.createElement('p');
        a.div.setAttribute('class', 'fib-item');
        p.textContent = `Fib(${treeDepth}) = ${a.val}`;
        a.div.appendChild(p);
        return a;

    }

    if (treeDepth == 1) {
        let b = {
            div: document.createElement('div'),
            val: 1
        }
        p = document.createElement('p');
        b.div.setAttribute('class', 'fib-item');
        p.textContent = `Fib(${treeDepth}) = ${b.val}`;
        b.div.appendChild(p);
        return b;

    }

//    Creates all the divs for the left 
    let div = document.createElement('div');
    div.setAttribute('class', 'fib-item');
    let left = fibonacci(treeDepth -1);
    left.div.setAttribute('class', 'fib-left fib-item');
    
//    Creates all the divs for the right
    let right = fibonacci(treeDepth -2);
    right.div.setAttribute('class', 'fib-right fib-item');
    txtDiv = document.createElement('p');
    txtDiv.textContent = `Fib(${treeDepth}) =${left.val + right.val}`;
    div.appendChild(txtDiv);
    div.appendChild(left.div);
    div.appendChild(right.div);
    return {
        div: div,
        val: (left.val + right.val)
    }

}

